import aboutData from '../../models/storeData.js';

function OptionThree(data) {
    return `<div id="tabs">
                <div class="about">
                    <h2>Option 3</h2>
                </div>
            </div>`;
  }

  document.querySelector('#tab').innerHTML = OptionThree(aboutData);

  export default OptionThree;