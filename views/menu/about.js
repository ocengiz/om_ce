import aboutData from '../../models/storeData.js';

function About(data) {
    return `<div id="tabs">
                <div class="about">
                    <h2>About</h2>
                    <div id="mobileEdit" class="tabButton"><ion-icon name="pencil-sharp"></ion-icon></div>
                </div>
                <div class="contact">
                    <div class="name1" style="font-weight: 700;"> ${data.firstName + " " + data.lastName} &nbsp;<span id="desktopPencil"><ion-icon name="pencil-sharp"></ion-icon>
                    <div id="modal">
                        <form action="">
                            <div class="input-field">
                                <input type="text" id="name" value= '${data.firstName + " " + data.lastName}' required />
                                <label for="name">NAME</label>
                                <section>
                                    <button type="button" id="save" onclick="saveHandlerDesktop(document.querySelectorAll('input'))">SAVE</button>
                                    <button type="button" id="cancel">CANCEL</button> 
                                </section>
                            </div>
                        </form> 
                    </div></span></div>
                    <div><span><ion-icon name="earth-sharp"></ion-icon></span>&nbsp;  ${data.website} &nbsp;<span id="desktopPencil2"><ion-icon name="pencil-sharp"></ion-icon>
                    <div id="modal2">
                        <form action="">
                            <div class="input-field">
                                <input type="text" id="website" value= ${data.website} required />
                                <label for="website">WEBSITE</label>
                                <section>
                                    <button type="button" id="save" onclick="saveHandlerDesktop(document.querySelectorAll('input'))">SAVE</button>
                                    <button type="button" id="cancel">CANCEL</button> 
                                </section>
                            </div>
                        </form> 
                    </div></span></div>
                    <div><span><ion-icon name="call-outline"></ion-icon></span>&nbsp; ${data.number} &nbsp;<span id="desktopPencil3"><ion-icon name="pencil-sharp"></ion-icon>
                    <div id="modal3">
                        <form action="">
                            <div class="input-field">
                                <input type="text" id="number" value= '${data.number}' required />
                                <label for="number">NUMBER</label>
                                <section>
                                    <button type="button" id="save" onclick="saveHandlerDesktop(document.querySelectorAll('input'))">SAVE</button>
                                    <button type="button" id="cancel">CANCEL</button> 
                                </section>
                            </div>
                        </form> 
                    </div></span></div>
                    <div><span><ion-icon name="home-outline"></ion-icon></span>&nbsp;  ${data.location} &nbsp;<span id="desktopPencil4"><ion-icon name="pencil-sharp"></ion-icon>
                    <div id="modal4">
                        <form action="">
                            <div class="input-field">
                                <input type="text" id="adress" value= '${data.location}' required />
                                <label for="adress">CITY, STATE & ZIP</label>
                                <section>
                                    <button type="button" id="save" onclick="saveHandlerDesktop(document.querySelectorAll('input'))">SAVE</button>
                                    <button type="button" id="cancel">CANCEL</button> 
                                </section>
                            </div>
                        </form> 
                    </div></span></div>
                </div>
                </div>`;
}

document.querySelector('#tab').innerHTML = About(aboutData);

export default About;