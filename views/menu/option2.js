import aboutData from '../../models/storeData.js';

function OptionTwo(data) {
    return `<div id="tabs">
                <div class="about">
                    <h2>Option 2</h2>
                </div>
            </div>`;
  }

  document.querySelector('#tab').innerHTML = OptionTwo(aboutData);

  export default OptionTwo;