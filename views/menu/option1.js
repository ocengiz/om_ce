import aboutData from '../../models/storeData.js';

function OptionOne(data) {
    return `<div id="tabs">
                <div class="about">
                    <h2>Option 1</h2>
                </div>
            </div>`;
  }

  document.querySelector('#tab').innerHTML = OptionOne(aboutData);

  export default OptionOne;