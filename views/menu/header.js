import aboutData from '../../models/storeData.js';

function Header(data) {
    return `<header>
    <div id="button"><button>LOG OUT</button></div>
    <button id="upload"><ion-icon name="camera"></ion-icon> Upload Cover Image</button>
    <div id="info">
        <img src="../resources/profile_image.jpg" alt="Profile Picture">
        <div id="info-contact">
            <h5> ${data.firstName + " " + data.lastName} </h5>
            <p style="order: 2;"><ion-icon name="call-outline"></ion-icon>  ${data.number} </p>
            <p style="order: 1;"><ion-icon name="location-outline"></ion-icon>  ${data.location} </p>
        </div>
        <div id="rate">
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star"></ion-icon>
            <ion-icon name="star-outline"></ion-icon>
            <p><span> ${data.review} </span>&nbsp; Reviews</p>
        </div>
        <p id="follow"><ion-icon name="add-circle"></ion-icon><span>${data.followers}</span>&nbsp; Followers</p>
    </div>
</header>
<div class="menu">
    <ul>
        <button class="active tabButton">ABOUT</button>
        <button class="tabButton">SETTINGS</button>
        <button class="tabButton">OPTION1</button>
        <button class="tabButton">OPTION2</button>
        <button class="tabButton">OPTION3</button>
    </ul>
</div>`;
}

document.querySelector('#head').innerHTML = Header(aboutData);

export default Header;