var aboutData = {
    website: 'www.seller.com',
    number: '(949) 325 - 68594',
    location: 'Newport Beach, CA',
    firstName: 'Jessica',
    lastName: 'Parker',
    review: 6,
    followers: 15,
};

export default aboutData;