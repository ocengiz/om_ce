import aboutData from '../models/storeData.js';

function EditInfo(data) {
    return `<div id="tabs">
    <div class="about">
        <h2>About</h2>
        <section>
            <button type="button" id="cancel">CANCEL</button>
            <button type="button" id="save" onclick="saveHandlerMobil(document.querySelectorAll('input'))">SAVE</button>
        </section>
    </div>
    <form >
        <div class="input-field">
          <input type="text" id="name" value=${data.firstName} />
          <label for="name">FIRST NAME</label>
        </div>
        <br>
        <div class="input-field">
            <input type="text" id="last" value=${data.lastName} />
            <label for="last">LAST NAME</label>
        </div>
        <br>
        <div class="input-field">
            <input type="text" id="website" value=${data.website} required />
            <label for="website">WEBSITE</label>
        </div>
        <br>
        <div class="input-field">
            <input type="text" id="number" value='${data.number}' required />
            <label for="number">PHONE NUMBER</label>
        </div>
        <br>
        <div class="input-field">
            <input type="text" id="adress" value='${data.location}' required />
            <label for="adress">CITY, STATE, ZIP</label>
        </div>
    </form>
</div>`;
}

document.querySelector('#tab').innerHTML = EditInfo(aboutData);

export default EditInfo;