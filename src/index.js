// Imports
import aboutData from '../models/storeData.js';
import Header from '../views/menu/header.js';
import About from '../views/menu/about.js';
import * as Handler from '../controller/handler.js';
import '../public /stylesheet/style.scss';

// Constants
const tab = document.querySelector('#tab');

window.onload = () => {
  // Calling modalHandler func
  Handler.ModalHandler();
  // Calling buttonHandler func
  Handler.ButtonHandler();
  // Calling the editHandler func
  Handler.EditHandler();
}
// Shared component
tab.innerHTML = About(aboutData);