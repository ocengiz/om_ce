import aboutData from '../models/storeData.js';
import About from '../views/menu/about.js';
import Settings from '../views/menu/settings.js';
import OptionOne from '../views/menu/option1.js';
import OptionTwo from '../views/menu/option2.js';
import OptionThree from '../views/menu/option3.js';
import EditInfo from '../models/edit.js';

const button = document.querySelectorAll('.tabButton');
const input = document.querySelectorAll('input');

function _e(id) {
  return document.getElementById(id);
}

// Edit button handler on mobile
export const EditHandler = () => {
  _e('mobileEdit').addEventListener('click', () => {
    _e('tab').innerHTML = EditInfo(aboutData);
      // Cancel Button
      _e('cancel').addEventListener('click', () => {
        _e('tab').innerHTML = About(aboutData);
        EditHandler();
        ModalHandler();
      });
      // Save Button
      _e('save').addEventListener('click', () => {
        _e('tab').innerHTML = About(aboutData);
        EditHandler();
        ModalHandler();
      });
  });
}
// Menu button handler, switching tabs
export const ButtonHandler = () => {
    // Every button on menu
    button.forEach(btn => {
        btn.addEventListener('click', function() {
          // get the active class when selecting the tab
          this.classList.add('active');
          for (let siblings of this.parentNode.children) {
            if(siblings !== this) siblings.classList.remove('active');
        }
      });
      // selecting the tab
      button[0].addEventListener('click', () => { 
        tab.innerHTML = About(aboutData);
        EditHandler();
        ModalHandler();
      });
      button[1].addEventListener('click', () => tab.innerHTML = Settings(aboutData));
      button[2].addEventListener('click', () => tab.innerHTML = OptionOne(aboutData));
      button[3].addEventListener('click', () => tab.innerHTML = OptionTwo(aboutData));
      button[4].addEventListener('click', () => tab.innerHTML = OptionThree(aboutData));
    });
}
  // Modal handler on desktop
export const ModalHandler = () => {
  // First 
  _e('desktopPencil').addEventListener('mouseover', () => {
    _e('modal').style.display = 'flex';
  });
  _e('desktopPencil').addEventListener('mouseout', () => {
    _e('modal').style.display = 'none';
  });
  // Second
  _e('desktopPencil2').addEventListener('mouseover', () => {
    _e('modal2').style.display = 'flex';
  });
  _e('desktopPencil2').addEventListener('mouseout', () => {
    _e('modal2').style.display = 'none';
  });
  // Third
  _e('desktopPencil3').addEventListener('mouseover', () => {
    _e('modal3').style.display = 'flex';
  });
  _e('desktopPencil3').addEventListener('mouseout', () => {
    _e('modal3').style.display = 'none';
  });
  // Fourth
  _e('desktopPencil4').addEventListener('mouseover', () => {
    _e('modal4').style.display = 'flex';
  });
  _e('desktopPencil4').addEventListener('mouseout', () => {
    _e('modal4').style.display = 'none';
  });
}

const saveHandlerMobil = (val) => { 
  // First name
  input[0].value = val[0];
  aboutData.firstName = val[0].value;
  // Last name
  input[1].value = val[1];
  aboutData.lastName = val[1].value;
  // Website
  input[2].value = val[2];
  aboutData.website = val[2].value;
  // Number
  input[3].value = val[3];
  aboutData.number = val[3].value;
  // Adress
  input[4].value = val[4];
  aboutData.location = val[4].value;
}

const saveHandlerDesktop = (val) => { 
  // Name
  input[0].value = val[0];
  aboutData.firstName = val[0].value;
  aboutData.lastName = "";
  // Website
  input[1].value = val[1];
  aboutData.website = val[1].value;
  // Number
  input[2].value = val[2];
  aboutData.number = val[2].value;
  // Adress
  input[3].value = val[3];
  aboutData.location = val[3].value;
  _e('tab').innerHTML = About(aboutData);
  EditHandler();
  ModalHandler();
}
// Scoping global
window.saveHandlerMobil = saveHandlerMobil;
window.saveHandlerDesktop = saveHandlerDesktop;